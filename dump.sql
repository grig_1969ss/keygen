-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: fstx
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category_order`
--

DROP TABLE IF EXISTS `category_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_order` (
  `order_id` bigint(20) unsigned NOT NULL,
  `part_type_id` int(10) unsigned NOT NULL,
  `machine_type_id` int(10) unsigned NOT NULL,
  KEY `category_order_order_id_foreign` (`order_id`),
  KEY `category_order_part_type_id_foreign` (`part_type_id`),
  KEY `category_order_machine_type_id_foreign` (`machine_type_id`),
  CONSTRAINT `category_order_machine_type_id_foreign` FOREIGN KEY (`machine_type_id`) REFERENCES `machine_type` (`id`),
  CONSTRAINT `category_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `category_order_part_type_id_foreign` FOREIGN KEY (`part_type_id`) REFERENCES `part_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_order`
--

LOCK TABLES `category_order` WRITE;
/*!40000 ALTER TABLE `category_order` DISABLE KEYS */;
INSERT INTO `category_order` VALUES (1,1,1),(2,1,1),(3,17,1),(4,5,1),(5,8,1),(6,1,1),(7,3,1);
/*!40000 ALTER TABLE `category_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Երևան',NULL,NULL),(2,'Աբովյան',NULL,NULL),(3,'Ագարակ',NULL,NULL),(4,'Ալավերդի',NULL,NULL),(5,'Ախթալա',NULL,NULL),(6,'Այրում',NULL,NULL),(7,'Աշտարակ',NULL,NULL),(8,'Ապարան',NULL,NULL),(9,'Արարատ',NULL,NULL),(10,'Արթիկ',NULL,NULL),(11,'Արմավիր',NULL,NULL),(12,'Արտաշատ',NULL,NULL),(13,'Բերդ',NULL,NULL),(14,'Բյուրեղավան',NULL,NULL),(15,'Գավառ',NULL,NULL),(16,'Գյումրի',NULL,NULL),(17,'Գորիս',NULL,NULL),(18,'Դաստակերտ',NULL,NULL),(19,'Դիլիջան',NULL,NULL),(20,'Եղեգնաձոր',NULL,NULL),(21,'Եղվարդ',NULL,NULL),(22,'Էջմիածին',NULL,NULL),(23,'Թալին',NULL,NULL),(24,'Թումանյան',NULL,NULL),(25,'Իջևան',NULL,NULL),(26,'Ծաղկաձոր',NULL,NULL),(27,'Կապան',NULL,NULL),(28,'Հրազդան',NULL,NULL),(29,'Ճամբարակ',NULL,NULL),(30,'Մասիս',NULL,NULL),(31,'Մարալիկ',NULL,NULL),(32,'Մարտունի',NULL,NULL),(33,'Մեծամոր',NULL,NULL),(34,'Մեղրի',NULL,NULL),(35,'Նոր հաճն',NULL,NULL),(36,'Նոյեմբերյան',NULL,NULL),(37,'Շամլուղ',NULL,NULL),(38,'Չարենցավան',NULL,NULL),(39,'Ջերմուկ',NULL,NULL),(40,'Սիսիան',NULL,NULL),(41,'Սպիտակ',NULL,NULL),(42,'Ստեփանավան',NULL,NULL),(43,'Սևան',NULL,NULL),(44,'Վայք',NULL,NULL),(45,'Վանաձոր',NULL,NULL),(46,'Վարդենիս',NULL,NULL),(47,'Վեդի',NULL,NULL),(48,'Տաշիր',NULL,NULL),(49,'Քաջարան',NULL,NULL);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_order`
--

DROP TABLE IF EXISTS `city_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_order` (
  `order_id` bigint(20) unsigned DEFAULT NULL,
  `city_id` int(10) unsigned NOT NULL,
  KEY `city_order_order_id_foreign` (`order_id`),
  KEY `city_order_city_id_foreign` (`city_id`),
  CONSTRAINT `city_order_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `city_order_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_order`
--

LOCK TABLES `city_order` WRITE;
/*!40000 ALTER TABLE `city_order` DISABLE KEYS */;
INSERT INTO `city_order` VALUES (1,1),(2,1),(3,2),(3,1),(4,1),(5,1),(6,1),(7,1),(7,2);
/*!40000 ALTER TABLE `city_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agree` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hvhh` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `FIO` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_email_unique` (`email`),
  UNIQUE KEY `hvhh` (`hvhh`),
  UNIQUE KEY `companies_hvhh_unique` (`hvhh`),
  KEY `companies_city_id_foreign` (`city_id`),
  CONSTRAINT `companies_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Company 1',1,'010111111',1,'grigor0704@gmail.com','87654321',1,NULL,'$2y$10$ZT3O3HWTzaBYwfCj6ROgzevGVCcFJpH.YjHkTOaD.gHdW7TuXGFK2','Address Line 1','Anun Azganun Hayranun',NULL,'2020-03-29 17:56:50','2020-05-12 11:53:29'),(2,'Company BMW',1,'010111111',1,'grigor@upsoftware.co.uk','25698745',1,NULL,'$2y$10$10OlYhk3VwU0d3jyZu9Ps.U42rnofxmxQnHWwZqB9I4FhbmVCaiue','Address Line 1','Anun Azganun Hayranun',NULL,'2020-03-29 18:19:48','2020-05-12 11:53:32'),(3,'Autopars',1,'091860786',1,'armen.eghiazaryan@gmail.com','77777700',0,NULL,'$2y$10$Z24.iXg3csNdNP0FuD/kVOj6RJ8FHmpmrZKzlrtnQ1iz/lhh4reM2','N. Tigranyan 14, Arabkir','Ani Badalyan',NULL,'2020-05-12 11:49:16','2020-06-18 12:42:14'),(4,'Zapchast ADZ',1,'099886655',1,'karentes@gmail.com','77777701',0,NULL,'$2y$10$0ww98zDFrWL7J7Mn3j40EOPYL/h2T4JEN3uWfLDw0caUuVtv42zG2','Gayi Poxota 16/5','Karen Karapetyan',NULL,'2020-05-15 12:11:37','2020-06-18 12:42:15'),(5,'Ավտոսան',1,'033833337',1,'harart67@mail.ru','00449313',0,NULL,'$2y$10$MZlHo.VYckGkTxtSkYLavutjtBFMnBLl1m9eWmMOswPsZGm7qTs1i','Ռուբինյանց 22 , Լենինգրադյան 31/18 , Դավիթ Բեկ 106/4','Պողոսյան Դավիթ',NULL,'2020-06-09 13:53:01','2020-06-18 12:42:17'),(6,'Lada Lend',1,'094244449',1,'levon.jagatspanyan.85@mail.ru','77777702',0,NULL,'$2y$10$aRplfzlmnliUfk2Bm33sAus1xaNVS4CYBF.qW3J8mvKhnbcbjE0E.','Zeytun, Rubinyanc 34','Lyov',NULL,'2020-06-09 13:59:00','2020-06-18 12:42:18'),(7,'Lada Lend',1,'094244449',1,'xcho_antonyan@mail.ru','77777703',0,NULL,'$2y$10$1eK0SUFrMiesP5XU45Yn1.947qwMA2iPzrllbs5uvhh98s9uyTbwG','Zeytun, Rubinyanc 34','Lyov',NULL,'2020-06-09 14:08:02','2020-06-18 12:42:19'),(8,'armautolamp',1,'043508585',1,'arm_autolamp@mail.ru','77777704',0,NULL,'$2y$10$SpnPZW07c4f7WJ6s5kmE/.qcHzTGTpLLDhxC7uQib5jdjg.DtgzGO','Багратунянц 93/20','Андраник Давтян',NULL,'2020-06-21 12:15:32','2020-06-22 21:54:59'),(9,'Avtopahestamaser Sebastiya 8',1,'033735333',1,'Harutyunnnn@gmail.com','77777705',0,NULL,'$2y$10$tuQIbSn4DqpDxXOInVLNFOMmidxbug3iY2ZgA3LMWoxU46E.yAH/q','Sebastiya 8','Harutyunyan Vahan',NULL,'2020-06-24 14:18:58','2020-06-24 14:18:58'),(10,'Edo Leningradyan',1,'077428599',1,'edgar.melqumyan@gmail.com','77777706',0,NULL,'$2y$10$BRZo5Kb1vWPilZdWWjjnEOai7OreF8ZSjtpIJjwUDrN4Pf0bC3MXa','Leningradyan 31/11','Edgar Melqumyan',NULL,'2020-06-24 15:40:47','2020-06-24 15:40:47'),(11,'Ohanov 1',1,'043323624',1,'d_arsenyan@inbox.ru','77777707',0,NULL,'$2y$10$aNOQqABE44JTqP9CKk00bOxIwOFjSiX26dI28EkuXxNRAllbU6QUO','Ohanov 1','Armen',NULL,'2020-06-25 06:57:53','2020-06-25 06:57:53'),(12,'SamKar',1,'077533577',1,'autoworld18@mail.ru','77777708',0,NULL,'$2y$10$YRpr1SAoLCQWTvrnrQ9UFelLuv63jA7VwJFRTUiCbWsJdtRnzdDEC','Nikoxayos Tigranyan 27/4','Artur Poxosyan',NULL,'2020-06-26 05:48:54','2020-06-26 05:48:54'),(13,'Vito Viano',1,'095222216',1,'avagnik555@rambler.ru','77777710',0,NULL,'$2y$10$k718BLmQuSOLUt1PDfJzwePkBg4kWBfFSx3lZ9DotKvOddLFebm5e','Davit Bek 180/27','Budumyan Armen',NULL,'2020-07-18 13:33:56','2020-07-18 13:33:56'),(14,'Mercedes Used Parts',1,'091326669',1,'artyomsimonyan85@gmail.com','77777712',0,NULL,'$2y$10$DPFU.7MtGwmdiQN2LI9bx.g8YZzke/ASbCj9bxDPPf6gUktO9BytK','Davitashen 4, 46/4 (35 I kanec)','Artyom Simonyan',NULL,'2020-07-25 10:36:09','2020-07-25 10:36:09'),(15,'Garage 33',1,'098027373',1,'haroutliz7@gmail.com','77777717',0,NULL,'$2y$10$8sBGfkzBjUuFCa.cPd703uHMAS.ogveCR0ZKaLP1YhaMBOKgxFS6O','Zovuni 194/1 to be updated','Artur Barsefyan',NULL,'2020-07-25 12:11:16','2020-07-25 12:11:16'),(16,'Mercedes-Benz',1,'077921022',1,'test2@gmail.com','77777714',0,NULL,'$2y$10$QZDKS0Zp6HzXxfJ73EAeEeDye98Gx02F06cF8HBNdzK43mvAHFOwK','Zovuni, Yeghvardi Xchuxi, 40th street','Gnel Arsenyan',NULL,'2020-07-29 13:11:38','2020-08-01 21:30:25'),(17,'Ավտոպահեստամասեր',43,'094454009',1,'evgine-hakobyan@mail.ru','77777715',0,NULL,'$2y$10$4cq83sJ/5VBc0jiBkswsIOs7nGl9a8ssvZWjq6RJPT7Tqc0c1WPAi','Խաղաղության 7','Հակոբ Ասատրյան',NULL,'2020-07-29 14:13:16','2020-07-29 14:13:16'),(18,'WASS Parts',1,'095886888',1,'armine.zaqaryan.90@list.ru','77777718',0,NULL,'$2y$10$bRFb3zm8KJXthIVfL/zKveOubqP8Zgh7hOqlIUCRcLjs0gxGmePDW','Tevosyan 16/9','Hovsepyan Karen',NULL,'2020-08-01 08:42:45','2020-08-01 08:42:45'),(19,'BMW Garage',1,'098776597',1,'edm171997@gmail.com','77777720',0,NULL,'$2y$10$tfahH3x6ia6MQb.bN4u14.k.OScdd.IXxkkdIo.wOVEnuSVD/w0ni','Rafa 51','Edgar Martirosyan',NULL,'2020-08-01 15:54:39','2020-08-01 15:54:39'),(20,'BMW Motors',1,'093800797 093898508',1,'gev-dav92@mail.ru','77777721',0,NULL,'$2y$10$2B8Ukl2VHX1YA3UFJPekHusDdJ/iCIMiomojTxWua3gcj6awt/43i','Silikyan Hin Xchuxi','Matos',NULL,'2020-08-02 12:20:30','2020-08-02 12:20:30'),(21,'Ավտոպահեստամասեր',11,'077339787',1,'test3@mail.ru','77777716',0,NULL,'$2y$10$SNpTlmUZ39X9hausxQF/w.DlDHxc1F9jL1nA.eylxUbG8b4UyL/mW','Աբովյան 8','Շմավոն Եփրեմյան',NULL,'2020-08-03 07:00:52','2020-08-03 07:00:52'),(22,'Ավտոպահեստամասեր',11,'+37477921616',1,'edgar.muradyan.98@inbox.ru','77777723',0,NULL,'$2y$10$h0L4HEaxXPBI0O2oxItA1.PNA2/nzyCOtRu6fJ4w74g2dgAQOvg16','Նորապատ 21','Էդգար Մուրադյան',NULL,'2020-08-03 10:47:06','2020-08-03 10:47:06'),(23,'Ավտոպահեստամասեր',45,'+37496111979',1,'goq-aslan@mail.ru','77777724',0,NULL,'$2y$10$cXxcTHhO/pxs383N17tiGurNAvZhdz5WhBjlAKaz1GS7qFcXEEzHW','Թումանյան 7','Գրիգոր Ասլանյան',NULL,'2020-08-04 07:43:56','2020-08-04 07:43:56'),(24,'Ավտոպահեստամասեր',46,'+37493683898',1,'armansargsyan1993.as@gmail.com','77777725',0,NULL,'$2y$10$KHK6k9blCRZDQ.du9BCkv.xHNG3a3WViDxH6EEmVwY4TFWKs1Rn9a','Երեվանյան 80','Ատոմ Սարգսյան',NULL,'2020-08-04 08:46:13','2020-08-04 08:46:13'),(25,'Ավտոպահեստամասեր',45,'+37493159000',1,'a-yegoryan@mail.ru','77777726',0,NULL,'$2y$10$/kLZCdTaLv3Oo/4W8j0sx.aTu.sOHXk2P/IYIY.U3uHEPQEVVEQ3C','Թումանյան 1/8','Արթուր Եգորյան',NULL,'2020-08-04 09:36:35','2020-08-04 09:36:35'),(26,'AGM PART',1,'094433103',1,'partbenz@mail.ru','77777730',0,NULL,'$2y$10$/50wTOs3vCUx7nYnNe7K7eLAsx7Ht2lqfet7V6IYJCgrmLI4RnQlG','Շիրակ Շարուրի խաչմերուկ','Արսեն Մարտիրոսյան',NULL,'2020-08-04 19:44:30','2020-08-04 19:44:30'),(27,'Ավտոպահեստամասեր',11,'+37498111813',1,'mxitardarbinyan@mail.ru','77777735',0,NULL,'$2y$10$jYTebWk70gmAEzmehFr16u3Dtf6q71.F07.LzxaQ.edV7XEYbzesi','Վարդան Այգեկցի 19/1','Սեվան Դարբինյան',NULL,'2020-09-04 08:39:21','2020-09-04 08:39:21');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_answers`
--

DROP TABLE IF EXISTS `company_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_answers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `producer` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_new` tinyint(1) NOT NULL DEFAULT '1',
  `is_original` tinyint(1) NOT NULL DEFAULT '0',
  `cost` int(11) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8mb4_unicode_ci,
  `company_id` int(10) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  `sub_order_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_answers_company_id_foreign` (`company_id`),
  KEY `company_answers_order_id_foreign` (`order_id`),
  CONSTRAINT `company_answers_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `company_answers_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_answers`
--

LOCK TABLES `company_answers` WRITE;
/*!40000 ALTER TABLE `company_answers` DISABLE KEYS */;
INSERT INTO `company_answers` VALUES (1,'artadrox 1',1,1,100000,NULL,1,2,'cnkadzev liser (Коленчатый вал)'),(2,'artadeox 1',1,1,250000,NULL,1,2,'cnkadzev liser (Коленчатый вал)'),(3,'zsgzsd',0,0,52343,NULL,1,2,'cnkadzev liser (Коленчатый вал)'),(4,'xcfgxdfs',0,1,2,NULL,1,2,'cnkadzev liser (Коленчатый вал)'),(5,'fxvxc',0,0,55555,NULL,1,2,'cnkadzev liser (Коленчатый вал)'),(6,'tets credentials',0,0,150000,'aqsasdasdasd',1,2,'cnkadzev liser (Коленчатый вал)'),(7,'test credentials',0,0,234234,'dfsdfsdf',1,2,'cnkadzev liser (Коленчатый вал)'),(8,'test credentials',0,0,234234,'dfsdfsdf',1,2,'cnkadzev liser (Коленчатый вал)'),(9,'credentials',0,0,234234,'dfsdfs',1,2,'cnkadzev liser (Коленчатый вал)'),(10,'credentials',0,0,324234,'dsdfsdf',1,2,'cnkadzev liser (Коленчатый вал)'),(11,'credentials',0,0,3,NULL,1,2,'cnkadzev liser (Коленчатый вал)'),(12,'credentials',0,0,222,'asdasdasd',1,2,'cnkadzev liser (Коленчатый вал)'),(13,'dfsdf',0,0,3333,'asdfsdf',1,2,'cnkadzev liser (Коленчатый вал)'),(14,'dfsdf',0,0,3333,'asdfsdf',1,2,'cnkadzev liser (Коленчатый вал)'),(15,'credentials',0,0,12312,'qweqwe',1,2,'cnkadzev liser (Коленчатый вал)'),(16,'sdfsdf',0,0,234,'234',1,2,'cnkadzev liser (Коленчатый вал)'),(17,'sdfsdf',0,0,234,'234',1,2,'cnkadzev liser (Коленчатый вал)'),(18,'sdfsdf',0,0,234,'234',1,2,'cnkadzev liser (Коленчатый вал)'),(19,'credentials',0,0,33322,'sdf',1,2,'cnkadzev liser (Коленчатый вал)'),(20,'credentials',0,0,342423,'sfsdfsdf',1,2,'cnkadzev liser (Коленчатый вал)'),(21,'masuma',1,0,38000,'patverov',7,5,'Ցապկի'),(22,'masuma',1,0,38000,'patverov',7,5,'Ցապկի'),(23,'test',1,1,12000,'test',1,2,'cnkadzev liser (Коленчатый вал)'),(24,'artadrox1',1,1,20000,'nor',1,2,'cnkadzev liser (Коленчатый вал)');
/*!40000 ALTER TABLE `company_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_machine_type`
--

DROP TABLE IF EXISTS `company_machine_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_machine_type` (
  `company_id` int(10) unsigned NOT NULL,
  `machine_type_id` int(10) unsigned DEFAULT NULL,
  KEY `company_machine_type_company_id_foreign` (`company_id`),
  KEY `company_machine_type_machine_type_id_foreign` (`machine_type_id`),
  CONSTRAINT `company_machine_type_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `company_machine_type_machine_type_id_foreign` FOREIGN KEY (`machine_type_id`) REFERENCES `machine_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_machine_type`
--

LOCK TABLES `company_machine_type` WRITE;
/*!40000 ALTER TABLE `company_machine_type` DISABLE KEYS */;
INSERT INTO `company_machine_type` VALUES (1,1),(2,1),(3,1),(3,2),(4,1),(5,1),(5,2),(6,1),(7,1),(8,1),(8,2),(9,1),(10,1),(11,1),(11,2),(12,1),(12,2),(13,1),(14,1),(15,1),(16,1),(17,1),(17,2),(18,1),(19,1),(20,1),(21,1),(21,2),(22,1),(22,2),(23,1),(23,2),(24,1),(24,2),(25,1),(25,2),(26,1),(27,1);
/*!40000 ALTER TABLE `company_machine_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_marks`
--

DROP TABLE IF EXISTS `company_marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_marks` (
  `company_id` int(10) unsigned NOT NULL,
  `mark_id` int(10) unsigned DEFAULT NULL,
  KEY `company_marks_company_id_foreign` (`company_id`),
  KEY `company_marks_mark_id_foreign` (`mark_id`),
  CONSTRAINT `company_marks_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `company_marks_mark_id_foreign` FOREIGN KEY (`mark_id`) REFERENCES `marks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_marks`
--

LOCK TABLES `company_marks` WRITE;
/*!40000 ALTER TABLE `company_marks` DISABLE KEYS */;
INSERT INTO `company_marks` VALUES (1,3),(1,5),(2,3),(2,5),(3,3),(3,5),(3,10),(3,11),(3,14),(3,20),(3,23),(3,26),(3,28),(3,30),(3,32),(3,33),(3,37),(3,41),(3,48),(3,52),(3,54),(3,55),(3,57),(3,69),(3,73),(3,74),(3,76),(3,77),(3,78),(3,79),(3,81),(3,82),(4,3),(4,5),(4,26),(4,30),(4,32),(4,33),(4,37),(4,41),(4,47),(4,48),(4,52),(4,54),(4,57),(4,61),(4,69),(4,73),(4,74),(4,78),(5,5),(5,10),(5,20),(5,26),(5,30),(5,32),(5,33),(5,37),(5,41),(5,47),(5,48),(5,52),(5,54),(5,57),(5,72),(5,73),(5,74),(5,78),(6,26),(6,30),(6,32),(6,37),(6,52),(6,54),(6,55),(6,57),(6,74),(6,77),(7,26),(7,30),(7,32),(7,37),(7,41),(7,47),(7,52),(7,54),(7,55),(7,57),(7,73),(7,74),(7,77),(8,5),(8,48),(9,3),(9,5),(9,26),(9,32),(9,37),(9,41),(9,47),(9,48),(9,51),(9,52),(9,54),(9,55),(9,57),(9,61),(9,69),(9,73),(9,74),(9,77),(9,78),(10,3),(10,5),(10,9),(10,10),(10,11),(10,14),(10,16),(10,19),(10,20),(10,24),(10,26),(10,29),(10,30),(10,32),(10,33),(10,35),(10,36),(10,37),(10,41),(10,45),(10,47),(10,48),(10,51),(10,52),(10,54),(10,57),(10,58),(10,60),(10,61),(10,62),(10,63),(10,64),(10,69),(10,72),(10,73),(10,74),(10,78),(10,79),(11,3),(11,5),(11,14),(11,20),(11,22),(11,26),(11,30),(11,37),(11,41),(11,47),(11,48),(11,52),(11,54),(11,55),(11,57),(11,62),(11,74),(11,76),(11,77),(11,78),(12,5),(12,10),(12,11),(12,20),(12,22),(12,48),(12,54),(12,55),(12,57),(12,74),(12,76),(12,78),(13,48),(14,48),(15,5),(15,48),(16,48),(17,3),(17,22),(17,26),(17,32),(17,41),(17,48),(17,52),(17,54),(17,55),(17,57),(17,69),(17,74),(17,77),(17,78),(17,82),(18,3),(18,57),(18,68),(18,69),(19,5),(20,5),(21,22),(21,26),(21,48),(21,54),(21,55),(21,57),(21,77),(21,78),(22,20),(22,48),(22,57),(22,77),(23,3),(23,5),(23,20),(23,22),(23,30),(23,32),(23,37),(23,41),(23,47),(23,48),(23,52),(23,54),(23,57),(23,74),(23,78),(23,82),(24,3),(24,22),(24,32),(24,41),(24,48),(24,52),(24,54),(24,55),(24,57),(24,74),(24,77),(24,78),(24,82),(25,3),(25,5),(25,20),(25,22),(25,26),(25,32),(25,37),(25,41),(25,47),(25,48),(25,52),(25,54),(25,55),(25,57),(25,74),(25,77),(25,78),(25,82),(26,48),(27,26),(27,30),(27,41),(27,52),(27,54),(27,74);
/*!40000 ALTER TABLE `company_marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_orders`
--

DROP TABLE IF EXISTS `company_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_orders` (
  `company_id` int(10) unsigned NOT NULL,
  `order_id` bigint(20) unsigned NOT NULL,
  KEY `company_orders_company_id_foreign` (`company_id`),
  KEY `company_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `company_orders_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `company_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_orders`
--

LOCK TABLES `company_orders` WRITE;
/*!40000 ALTER TABLE `company_orders` DISABLE KEYS */;
INSERT INTO `company_orders` VALUES (1,2),(3,4),(4,4),(3,5),(5,5),(6,5),(7,5),(1,6),(2,6);
/*!40000 ALTER TABLE `company_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_part_type`
--

DROP TABLE IF EXISTS `company_part_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_part_type` (
  `company_id` int(10) unsigned NOT NULL,
  `part_type_id` int(10) unsigned DEFAULT NULL,
  KEY `company_part_type_company_id_foreign` (`company_id`),
  KEY `company_part_type_part_type_id_foreign` (`part_type_id`),
  CONSTRAINT `company_part_type_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `company_part_type_part_type_id_foreign` FOREIGN KEY (`part_type_id`) REFERENCES `part_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_part_type`
--

LOCK TABLES `company_part_type` WRITE;
/*!40000 ALTER TABLE `company_part_type` DISABLE KEYS */;
INSERT INTO `company_part_type` VALUES (1,1),(1,3),(1,4),(2,1),(2,3),(2,4),(3,1),(3,2),(3,3),(3,4),(3,5),(3,6),(3,7),(3,8),(3,9),(3,10),(3,11),(3,12),(3,15),(3,16),(3,17),(4,1),(4,2),(4,5),(4,7),(4,10),(4,11),(5,1),(5,2),(5,3),(5,4),(5,5),(5,6),(5,7),(5,8),(5,9),(5,10),(5,11),(5,12),(5,13),(6,1),(6,3),(6,4),(6,5),(6,6),(6,7),(6,8),(6,9),(6,10),(6,11),(6,12),(6,16),(6,17),(7,1),(7,2),(7,3),(7,4),(7,5),(7,6),(7,7),(7,8),(7,9),(7,10),(7,11),(7,12),(7,16),(7,17),(8,11),(8,16),(8,17),(9,1),(9,2),(9,3),(9,4),(9,5),(9,6),(9,7),(9,8),(9,10),(9,11),(9,17),(10,1),(10,2),(10,3),(10,4),(10,5),(10,6),(10,7),(10,8),(10,9),(10,10),(10,11),(10,12),(10,13),(10,14),(10,15),(10,16),(10,17),(11,1),(11,2),(11,3),(11,5),(11,7),(11,9),(11,11),(11,12),(11,13),(11,15),(11,16),(11,17),(12,1),(12,2),(13,1),(14,1),(14,2),(14,3),(14,4),(14,5),(14,6),(14,7),(14,8),(14,9),(14,10),(14,11),(14,12),(14,13),(14,14),(14,15),(14,16),(14,17),(15,1),(15,2),(15,3),(15,4),(15,5),(15,6),(15,7),(15,8),(15,10),(15,11),(15,12),(15,13),(15,14),(15,15),(15,16),(15,17),(16,1),(16,2),(16,3),(16,4),(16,5),(16,6),(16,7),(16,8),(16,9),(16,10),(16,11),(16,12),(16,13),(16,14),(16,15),(16,16),(16,17),(17,1),(17,2),(17,3),(17,5),(17,6),(17,7),(17,8),(17,9),(17,10),(17,11),(18,1),(18,2),(18,3),(18,4),(18,5),(18,6),(18,7),(18,8),(18,9),(18,10),(18,11),(18,12),(18,13),(18,14),(18,15),(18,16),(18,17),(19,1),(19,2),(19,3),(19,4),(19,6),(19,7),(19,8),(19,9),(19,10),(19,11),(19,12),(19,13),(19,14),(19,15),(19,16),(19,17),(20,1),(20,2),(20,3),(20,4),(20,5),(20,6),(20,7),(20,8),(20,9),(20,10),(20,11),(20,12),(20,13),(20,14),(20,15),(20,16),(20,17),(21,1),(21,2),(21,3),(21,5),(21,6),(21,7),(21,8),(22,1),(22,2),(22,3),(22,4),(22,5),(22,6),(22,7),(22,8),(22,9),(22,10),(22,11),(22,12),(22,13),(22,14),(22,15),(22,16),(22,17),(23,1),(23,2),(23,3),(23,4),(23,5),(23,6),(23,7),(23,8),(23,9),(23,10),(23,11),(23,12),(23,17),(24,1),(24,2),(24,3),(24,4),(24,5),(24,6),(24,7),(24,8),(24,9),(24,10),(24,11),(24,12),(24,14),(24,16),(24,17),(25,1),(25,2),(25,3),(25,4),(25,5),(25,6),(25,7),(25,8),(25,9),(25,10),(25,11),(25,12),(25,13),(25,14),(25,15),(25,16),(25,17),(26,8),(26,9),(26,10),(26,11),(26,12),(26,13),(26,14),(26,15),(26,16),(26,17),(27,1),(27,2),(27,3),(27,4),(27,5),(27,6),(27,7),(27,8),(27,10),(27,12),(27,13),(27,14),(27,15);
/*!40000 ALTER TABLE `company_part_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` bigint(20) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `machine_type`
--

DROP TABLE IF EXISTS `machine_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `machine_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machine_type`
--

LOCK TABLES `machine_type` WRITE;
/*!40000 ALTER TABLE `machine_type` DISABLE KEYS */;
INSERT INTO `machine_type` VALUES (1,'Մարդատար',NULL,NULL),(2,'Բեռնատար',NULL,NULL),(3,'Մոտոտեխնիկա',NULL,NULL),(4,'Հատուկ տեխնիկա',NULL,NULL),(5,'Ավտոբուս',NULL,NULL),(6,'Կցասայլ',NULL,NULL),(7,'Ջրային տեխնիկա',NULL,NULL);
/*!40000 ALTER TABLE `machine_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marks`
--

DROP TABLE IF EXISTS `marks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marks`
--

LOCK TABLES `marks` WRITE;
/*!40000 ALTER TABLE `marks` DISABLE KEYS */;
INSERT INTO `marks` VALUES (1,'Acura',NULL,NULL),(2,'Alfa Romeo',NULL,NULL),(3,'Audi',NULL,NULL),(4,'Bentley',NULL,NULL),(5,'BMW',NULL,NULL),(6,'Buick',NULL,NULL),(7,'BYD',NULL,NULL),(8,'Cadillac',NULL,NULL),(9,'Chery',NULL,NULL),(10,'Chevrolet',NULL,NULL),(11,'Chrysler',NULL,NULL),(12,'Citroen',NULL,NULL),(13,'Dacia',NULL,NULL),(14,'Daewoo',NULL,NULL),(15,'Daihatsu',NULL,NULL),(16,'Dodge',NULL,NULL),(17,'Dong Feng',NULL,NULL),(18,'Ferrari',NULL,NULL),(19,'Fiat',NULL,NULL),(20,'Ford',NULL,NULL),(21,'Foton',NULL,NULL),(22,'GAZ/ГАЗ',NULL,NULL),(23,'Geely',NULL,NULL),(24,'GMC',NULL,NULL),(25,'Great Wall',NULL,NULL),(26,'Honda',NULL,NULL),(27,'Hongxing',NULL,NULL),(28,'Huanghai',NULL,NULL),(29,'Hummer',NULL,NULL),(30,'Hyundai',NULL,NULL),(31,'Ikco Iran',NULL,NULL),(32,'Infiniti',NULL,NULL),(33,'Isuzu',NULL,NULL),(34,'IZH/ИЖ',NULL,NULL),(35,'Jaguar',NULL,NULL),(36,'Jeep',NULL,NULL),(37,'Kia',NULL,NULL),(38,'Lamborghini',NULL,NULL),(39,'Lancia',NULL,NULL),(40,'Land Rover',NULL,NULL),(41,'Lexus',NULL,NULL),(42,'Lincoln',NULL,NULL),(43,'LuAZ',NULL,NULL),(44,'Mahindra',NULL,NULL),(45,'Maserati',NULL,NULL),(46,'Maybach',NULL,NULL),(47,'Mazda',NULL,NULL),(48,'Mercedes-Benz',NULL,NULL),(49,'Mercury',NULL,NULL),(50,'MG',NULL,NULL),(51,'MINI',NULL,NULL),(52,'Mitsubishi',NULL,NULL),(53,'Moskvich/Москвич',NULL,NULL),(54,'Nissan',NULL,NULL),(55,'Niva/Нива',NULL,NULL),(56,'OKA',NULL,NULL),(57,'Opel',NULL,NULL),(58,'Peugeot',NULL,NULL),(59,'Plymouth',NULL,NULL),(60,'Pontiac',NULL,NULL),(61,'Porsche',NULL,NULL),(62,'Renault',NULL,NULL),(63,'Rolls Royce',NULL,NULL),(64,'Rover',NULL,NULL),(65,'Sapia',NULL,NULL),(66,'Saturn',NULL,NULL),(67,'Scion',NULL,NULL),(68,'Seat',NULL,NULL),(69,'Skoda',NULL,NULL),(70,'Smart',NULL,NULL),(71,'Sang-yong',NULL,NULL),(72,'Subaru',NULL,NULL),(73,'Suzuki',NULL,NULL),(74,'Toyota',NULL,NULL),(75,'Trabant',NULL,NULL),(76,'UAZ',NULL,NULL),(77,'VAZ/ВАЗ/Lada',NULL,NULL),(78,'Volkswagen',NULL,NULL),(79,'Volvo',NULL,NULL),(80,'Wartburg',NULL,NULL),(81,'ZAZ/ЗАЗ',NULL,NULL),(82,'ZIL/ЗИЛ',NULL,NULL);
/*!40000 ALTER TABLE `marks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_12_073520_create_cities_table',1),(4,'2019_08_12_074754_create_companies_table',1),(5,'2019_08_14_082430_create_orders_table',1),(6,'2019_08_15_123452_create_jobs_table',1),(7,'2019_08_20_085639_create_part_type_table',1),(8,'2019_08_20_143149_create_machine_type_table',1),(9,'2019_08_20_144947_create_city_order_table',1),(10,'2019_08_20_145450_create_category_order_table',1),(11,'2019_08_20_190014_create_company_machine_type_table',1),(12,'2019_08_22_061429_create_company_part_type_table',1),(13,'2019_08_22_080701_create_marks_table',1),(14,'2019_08_22_083403_create_company_marks_table',1),(15,'2019_09_21_162024_orders_rename_email',1),(16,'2019_09_21_171619_create_failed_jobs_table',1),(17,'2019_10_19_100442_create_company_order',1),(18,'2019_10_26_082336_create_company_answers_table',1),(19,'2019_11_10_124405_create_sub_orders_table',1),(20,'2019_12_10_112741_change_column_int_to_string_in_orders_table',1),(21,'2019_12_19_093440_add_column_agree_in_orders_table',1),(22,'2019_12_24_143310_add_column_agree_in_companies_table',1),(23,'2019_12_28_073542_add_description_to_company_answers_table',1),(24,'2020_01_12_112635_add_hvhh_and_is_active_columns_to_companies_table',1),(25,'2020_01_13_102302_add_column_is_original_in_company_answers_table',1),(26,'2020_01_14_155116_change_motor_liter_type_in_orders_table',1),(27,'2020_01_18_082819_make_hvhh_column_unique_in_companies_table',1),(28,'2020_01_29_112714_add_sub_order_id_column_in_company_answers_table',1),(29,'2020_02_04_165659_create_test_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int(11) NOT NULL,
  `mark_id` int(11) NOT NULL,
  `modification` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `motor_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `motor_liter` decimal(5,1) NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transmission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `drive_unit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_phone` int(11) DEFAULT NULL,
  `to_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `radios` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_1` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `agree` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,NULL,2009,5,'e60','Բենզին',3.0,'Սեդան','automatic','full','WBA125s5desd2sw',0,'ggrig93@gmail.com','i_have','Grigor','094 880704',NULL,'ggrig93@gmail.com','2020-03-29 17:49:19','2020-03-29 17:49:19',1),(2,1,NULL,2009,5,'e60','Բենզին',3.0,'Սեդան','automatic','full','Wba3rhnmzks6215',0,'ggrig93@gmail.com','i_have','Grigor','094 880704',NULL,'ggrig93@gmail.com','2020-03-29 17:59:57','2020-03-29 17:59:57',1),(3,NULL,'xdg',2020,1,'ererwers','Բենզին',4.0,'Սեդան','automatic','back','ertertert',0,'ggrig93@gmail.com','without','rsfssdfwer','094 880704',NULL,'ggrig93@gmail.com','2020-05-08 14:05:28','2020-05-08 14:05:28',1),(4,NULL,NULL,2015,74,'camry','Բենզին',2.5,'Սեդան','automatic','back','asd',0,'dave8387@gmail.com','without','Poxos','+374 93 830087',NULL,'dave8387@gmail.com','2020-06-09 13:26:31','2020-06-09 13:26:31',1),(5,NULL,NULL,2009,54,'Murano','Բենզին',3.5,'Հետչբեք','automatic','back','JN8AZ18W49W160793',0,'vahan.smbatyan@yahoo.com','without','Vahan','041 491525',NULL,'vahan.smbatyan@yahoo.com','2020-06-10 08:32:55','2020-06-10 08:32:55',1),(6,1,'asdsd',2020,5,'e60','Բենզին',3.0,'Սեդան','automatic','back','asdsdasd',0,'ggrig93@gmail.com','i_have','Grigor','094 880704',NULL,'ggrig93@gmail.com','2020-06-18 12:45:44','2020-06-18 12:45:44',1),(7,NULL,'Indz arag ed petqa',2018,74,'camry','Բենզին',3.0,'Սեդան','automatic','front','81917289291010',0,'dzknikloxturloxara@bldux.com','without','vardges serobic','093 123456',NULL,'dzknikloxturloxara@bldux.com','2020-09-11 19:44:54','2020-09-11 19:44:54',1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `part_type`
--

DROP TABLE IF EXISTS `part_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `part_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `part_type`
--

LOCK TABLES `part_type` WRITE;
/*!40000 ALTER TABLE `part_type` DISABLE KEYS */;
INSERT INTO `part_type` VALUES (1,'Շարժիչ',NULL,NULL),(2,'Թափք',NULL,NULL),(3,'Փոխանցման տուփ',NULL,NULL),(4,'Ղեկի կառավարում',NULL,NULL),(5,'Արգելակման համակարգեր',NULL,NULL),(6,'Արտանետման համակարգեր',NULL,NULL),(7,'Վառելիքային համակարգեր',NULL,NULL),(8,'Կախոցներ',NULL,NULL),(9,'Մարտկոցներ',NULL,NULL),(10,'Էլեկտրական համակարգեր',NULL,NULL),(11,'Լուսավորում և օպտիկա',NULL,NULL),(12,'Ավտոսրահի մասեր',NULL,NULL),(13,'Ապակիներ',NULL,NULL),(14,'Անիվներ և անվադողեր',NULL,NULL),(15,'Աուդիոտեխնիկա',NULL,NULL),(16,'Ավտոէլեկտրոնիկա',NULL,NULL),(17,'Աքսեսուարներ',NULL,NULL);
/*!40000 ALTER TABLE `part_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('ggrig93@gmail.com','$2y$10$Wh5.ysWTqEpcLI2QXx5paua3ff.VmNQBiLQ2xEQnLSJzbn/1W5Xn2','2021-01-01 23:31:24');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_orders`
--

DROP TABLE IF EXISTS `sub_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) unsigned NOT NULL,
  `part_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `part_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catalog_number` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sub_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `sub_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_orders`
--

LOCK TABLES `sub_orders` WRITE;
/*!40000 ALTER TABLE `sub_orders` DISABLE KEYS */;
INSERT INTO `sub_orders` VALUES (1,1,'cnkadzev liser (Коленчатый вал)','original_type','new_status',NULL,NULL,'2020-03-29 17:49:19','2020-03-29 17:49:19'),(2,2,'cnkadzev liser (Коленчатый вал)','original_type','new_status',NULL,NULL,'2020-03-29 17:59:57','2020-03-29 17:59:57'),(3,3,'shit','any_type','any_status',NULL,NULL,'2020-05-08 14:05:28','2020-05-08 14:05:28'),(4,4,'kalodka','any_type','any_status',NULL,NULL,'2020-06-09 13:26:31','2020-06-09 13:26:31'),(5,5,'Ցապկի','any_type','any_status',NULL,NULL,'2020-06-10 08:32:55','2020-06-10 08:32:55'),(6,6,'test','any_type','any_status',NULL,NULL,'2020-06-18 12:45:44','2020-06-18 12:45:44'),(7,7,'կաշի','any_type','new_status',NULL,NULL,'2020-09-11 19:44:54','2020-09-11 19:44:54');
/*!40000 ALTER TABLE `sub_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_phone_unique` (`phone`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Grigor','094880704','ggrig93@gmail.com',NULL,'$2y$10$ignln5xLoAWtaxeU7nW3SeiA2lNMtSSZCChEJ13adVutrI30grTg.',NULL,'2020-03-22 12:36:17','2020-03-22 12:36:17');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-09 15:17:36
